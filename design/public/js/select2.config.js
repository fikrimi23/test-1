var ajaxIdSelect2Config = function (types = null) {
    var types_select = types;
    return {
        'width': '100%',
        'theme': 'bootstrap',
        allowClear: true,
        ajax: {
            url: $('input[name="tourleader-url"]').val(),
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                    tourleader_status: types_select
                };
        },
        processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: $.map(data.items, function (item, index) {
                        return {
                            text: item.id + " - " + item.fullname + ' (' + item.status_name + ')',
                            id: item.id
                        }
                    }),
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
        },
        cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        // templateResult: formatRepo, // omitted for brevity, see the source of this page
        // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    }
}

var ajaxMaterialSelect2Config = function () {
    return {
        'width': '100%',
        'theme': 'bootstrap',
        placeholder: "Kode Barang",
        allowClear: false,
        ajax: {
            url: $('input[name="material-url"]').val(),
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
        },
        processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: $.map(data.items, function (item, index) {
                        return {
                            text: item.id + " - " + item.name,
                            id: item.id
                        }
                    }),
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
        },
        cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        // templateResult: formatRepo, // omitted for brevity, see the source of this page
        // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    }
}

var ajaxVendorSelect2Config = function () {
    return {
        'width': '100%',
        'theme': 'bootstrap',
        placeholder: "Kode Suplier",
        allowClear: false,
        ajax: {
            url: $('input[name="vendor-url"]').val(),
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
        },
        processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: $.map(data.items, function (item, index) {
                        return {
                            text: item.id + " - " + item.name,
                            id: item.id
                        }
                    }),
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
        },
        cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        // templateResult: formatRepo, // omitted for brevity, see the source of this page
        // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    }
}

var ajaxMaterialProductionSelect2Config = function (vendor_id) {
    return {
        'width': '100%',
        'theme': 'bootstrap',
        placeholder: "Kode Barang",
        allowClear: false,
        ajax: {
            url: $('input[name="material-url"]').val(),
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                    vendor_id: vendor_id
                };
        },
        processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: $.map(data.items, function (item, index) {
                        return {
                            text: item.id + " - " + item.material.material_name,
                            id: item.id
                        }
                    }),
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
        },
        cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        // templateResult: formatRepo, // omitted for brevity, see the source of this page
        // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    }
}
